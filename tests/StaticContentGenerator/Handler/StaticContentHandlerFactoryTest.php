<?php

declare(strict_types=1);

namespace Interitty\StaticContentGenerator\Handler;

use Interitty\PhpUnit\BaseTestCase;
use Interitty\StaticContentGenerator\Storage\StorageInterface;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\StaticContentGenerator\Handler\StaticContentHandlerFactory
 */
class StaticContentHandlerFactoryTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of StaticContentHandler factory
     *
     * @return void
     * @covers ::create
     */
    public function testFactory(): void
    {
        $filename = 'filename';
        $storage = $this->createStorageMock();
        $handlerFactory = $this->createStaticContentHandlerFactory();
        $handler = $handlerFactory->create($storage, $filename, true);

        self::assertSame($storage, $this->callNonPublicMethod($handler, 'getStorage'));
        self::assertSame($filename, $this->callNonPublicMethod($handler, 'getFilename'));
        self::assertTrue($this->callNonPublicMethod($handler, 'isMuteOutput'));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * StaticContentHandlerFactory factory
     *
     * @return StaticContentHandlerFactory
     */
    protected function createStaticContentHandlerFactory(): StaticContentHandlerFactory
    {
        $handlerFactory = new StaticContentHandlerFactory();
        return $handlerFactory;
    }

    /**
     * Storage mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return StorageInterface&MockObject
     */
    protected function createStorageMock(array $methods = []): StorageInterface&MockObject
    {
        $mock = $this->createMockAbstract(StorageInterface::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
