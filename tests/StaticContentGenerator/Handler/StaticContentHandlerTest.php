<?php

declare(strict_types=1);

namespace Interitty\StaticContentGenerator\Handler;

use Interitty\PhpUnit\BaseTestCase;
use Interitty\StaticContentGenerator\Storage\StorageInterface;
use LogicException;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\StaticContentGenerator\Handler\StaticContentHandler
 */
class StaticContentHandlerTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of simple Integration all services
     *
     * @return void
     * @group integration
     * @covers ::processBegin
     * @covers ::processEnd
     * @covers ::processManageOutput
     * @covers ::getContent
     */
    public function testSimpleIntegration(): void
    {
        $content = 'content';
        $filename = 'filename';
        $muteOutput = false;
        $storage = $this->createStorageMock(['processPutContent']);
        $storage->expects(self::once())
            ->method('processPutContent')
            ->with(self::equalTo($filename), self::equalTo($content));
        $handler = $this->createHandler($storage, $filename, $muteOutput);

        self::assertNull($this->callNonPublicMethod($handler, 'getContent'));

        $handler->processBegin();
        self::assertSame('', $this->callNonPublicMethod($handler, 'getContent'));

        $handler->processManageOutput($content);
        self::assertSame($content, $this->callNonPublicMethod($handler, 'getContent'));

        $handler->processEnd();
        self::assertNull($this->callNonPublicMethod($handler, 'getContent'));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of constructor
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstructor(): void
    {
        $filename = 'filename';
        $storage = $this->createStorageMock();
        $handler = $this->createHandler($storage, $filename, true);
        self::assertSame($storage, $this->callNonPublicMethod($handler, 'getStorage'));
        self::assertSame($filename, $this->callNonPublicMethod($handler, 'getFilename'));
        self::assertTrue($this->callNonPublicMethod($handler, 'isMuteOutput'));
    }

    /**
     * Tester of Filename getter/setter implementation
     *
     * @param string $filename
     * @return void
     * @covers ::getFilename
     * @covers ::setFilename
     * @dataProvider stringDataProvider
     */
    public function testGetSetFilename(string $filename): void
    {
        $this->processTestGetSet(StaticContentHandler::class, 'filename', $filename);
    }

    /**
     * Tester of HandlerName getter implementation
     *
     * @return void
     * @covers ::getHandlerName
     */
    public function testGetHandlerName(): void
    {
        $handler = $this->createHandlerMock(['getFilename']);
        $handler->expects(self::once())
            ->method('getFilename')
            ->willReturn('filename');
        self::assertSame('StaticContentHandler-filename', $handler->getHandlerName());
    }

    /**
     * Tester of MuteOutput checker/setter implementation
     *
     * @return void
     * @covers ::isMuteOutput
     * @covers ::setMuteOutput
     */
    public function testGetCheckMuteOutput(): void
    {
        $className = StaticContentHandler::class;
        $propertyName = 'muteOutput';

        $this->processTestGetBoolDefault($className, $propertyName, false);
        $this->processTestGetSetBool($className, $propertyName, true);
        $this->processTestGetSetBool($className, $propertyName, false);
    }

    /**
     * Tester of Storage getter/setter implementation
     *
     * @return void
     * @covers ::getStorage
     * @covers ::setStorage
     */
    public function testGetSetStorage(): void
    {
        $storage = $this->createStorageMock();
        $this->processTestGetSet(StaticContentHandler::class, 'storage', $storage);
    }

    /**
     * Tester of processBegin already begun
     *
     * @return void
     * @covers ::processBegin
     * @group negative
     */
    public function testProcessBeginAlreadyBegun(): void
    {
        $handler = $this->createHandlerMock();
        $handler->processBegin();

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Content storage was already initialized.');
        $handler->processBegin();
    }

    /**
     * Tester of processEnd with empty content
     *
     * @return void
     * @covers ::processEnd
     */
    public function testProcessEndEmptyContent(): void
    {
        $content = '';
        $filename = 'filename';
        $storage = $this->createStorageMock(['processPutContent']);
        $storage->expects(self::once())
            ->method('processPutContent')
            ->with(self::equalTo($filename), self::equalTo($content));
        $handler = $this->createHandlerMock(['getFilename', 'getStorage']);
        $handler->expects(self::once())
            ->method('getFilename')
            ->willReturn($filename);
        $handler->expects(self::once())
            ->method('getStorage')
            ->willReturn($storage);

        self::assertNull($this->getNonPublicPropertyValue($handler, 'content'));
        $handler->processBegin();
        self::assertSame($content, $this->getNonPublicPropertyValue($handler, 'content'));
        $handler->processEnd();
        self::assertNull($this->getNonPublicPropertyValue($handler, 'content'));
    }

    /**
     * Tester of processEnd not begun
     *
     * @return void
     * @covers ::processEnd
     */
    public function testProcessEndNotBegun(): void
    {
        $handler = $this->createHandlerMock(['getFilename', 'getStorage']);
        $handler->expects(self::never())
            ->method('getFilename');
        $handler->expects(self::never())
            ->method('getStorage');

        self::assertNull($this->getNonPublicPropertyValue($handler, 'content'));
        $handler->processEnd();
    }

    /**
     * Tester of processManageOutput
     *
     * @return void
     * @covers ::processManageOutput
     */
    public function testProcessManageOutput(): void
    {
        $content = 'content';
        $handler = $this->createHandlerMock(['isMuteOutput']);
        $handler->expects(self::exactly(2))
            ->method('isMuteOutput')
            ->willReturn(false);

        $handler->processBegin();
        self::assertSame('', $this->callNonPublicMethod($handler, 'getContent'));

        self::assertSame($content, $handler->processManageOutput($content));
        self::assertSame($content, $this->callNonPublicMethod($handler, 'getContent'));

        /**
         * @phpstan-var string $content
         * @phpstan-ignore varTag.nativeType
         */
        self::assertSame($content, $handler->processManageOutput($content));
        self::assertSame($content . $content, $this->callNonPublicMethod($handler, 'getContent'));
    }

    /**
     * Tester of processManageOutput muted
     *
     * @return void
     * @covers ::processManageOutput
     */
    public function testProcessManageMuteOutput(): void
    {
        $content = 'content';
        $handler = $this->createHandlerMock(['isMuteOutput']);
        $handler->expects(self::exactly(2))
            ->method('isMuteOutput')
            ->willReturn(true);

        $handler->processBegin();
        self::assertSame('', $this->callNonPublicMethod($handler, 'getContent'));

        self::assertSame('', $handler->processManageOutput($content));
        self::assertSame($content, $this->callNonPublicMethod($handler, 'getContent'));

        self::assertSame('', $handler->processManageOutput($content));
        self::assertSame($content . $content, $this->callNonPublicMethod($handler, 'getContent'));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * StaticContentHandler factory
     *
     * @param StorageInterface $storage
     * @param string $filename
     * @param bool $muteOutput
     * @return StaticContentHandler
     */
    protected function createHandler(
        StorageInterface $storage,
        string $filename,
        bool $muteOutput = false
    ): StaticContentHandler {
        $handler = new StaticContentHandler($storage, $filename, $muteOutput);
        return $handler;
    }

    /**
     * StaticContentHandler mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return StaticContentHandler&MockObject
     */
    protected function createHandlerMock(array $methods = []): StaticContentHandler&MockObject
    {
        $mock = $this->createPartialMock(StaticContentHandler::class, $methods);
        return $mock;
    }

    /**
     * Storage mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return StorageInterface&MockObject
     */
    protected function createStorageMock(array $methods = []): StorageInterface&MockObject
    {
        $mock = $this->createMockAbstract(StorageInterface::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
