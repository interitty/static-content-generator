<?php

declare(strict_types=1);

namespace Interitty\StaticContentGenerator;

use Generator;
use Interitty\OutputBufferManager\OutputBufferManager;
use Interitty\PhpUnit\BaseTestCase;
use Interitty\StaticContentGenerator\Handler\StaticContentHandler;
use Interitty\StaticContentGenerator\Handler\StaticContentHandlerFactory;
use Interitty\StaticContentGenerator\Handler\StaticContentHandlerFactoryInterface;
use Interitty\StaticContentGenerator\Storage\StorageInterface;
use Nette\Utils\AssertionException;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\StaticContentGenerator\StaticContentGenerator
 * @phpstan-import-type AssertionExceptionData from BaseTestCase
 */
class StaticContentGeneratorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of isHandlerInProgress checker
     *
     * @return void
     * @group integration
     * @covers ::isHandlerInProgress
     */
    public function testIsInProgress(): void
    {
        $outputBufferManager = $this->createOutputBufferManager();
        $storage = $this->createStorageMock();
        $handler = $this->createHandlerMock(['getHandlerName', 'processEnd']);
        $handler->expects(self::any())
            ->method('getHandlerName')
            ->willReturn('handlerMock');
        $handler->expects(self::once())
            ->method('processEnd');
        $generator = $this->createGenerator($outputBufferManager, $storage);
        $generator->setHandler($handler);

        self::assertFalse($generator->isHandlerInProgress());
        $generator->processBegin();
        self::assertTrue($generator->isHandlerInProgress());
        $generator->processEnd();
        self::assertFalse($generator->isHandlerInProgress());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of constructor
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstructor(): void
    {
        $outputBufferManager = $this->createOutputBufferManagerMock();
        $storage = $this->createStorageMock();
        $generator = $this->createGenerator($outputBufferManager, $storage);
        self::assertSame($outputBufferManager, $this->callNonPublicMethod($generator, 'getOutputBufferManager'));
        self::assertSame($storage, $this->callNonPublicMethod($generator, 'getStorage'));
    }

    /**
     * Tester of StaticContentHandler factory
     *
     * @return void
     * @covers ::createHandler
     */
    public function testCreateHandler(): void
    {
        $filename = 'filename';
        $storage = $this->createStorageMock();
        $handler = $this->createHandlerMock();
        $handlerFactory = $this->createHandlerFactoryMock(['create']);
        $handlerFactory->expects(self::once())
            ->method('create')
            ->with(self::equalTo($storage), self::equalTo($filename))
            ->willReturn($handler);
        $outputBufferManager = $this->createOutputBufferManagerMock();
        $generator = $this->createGenerator($outputBufferManager, $storage);
        $generator->setHandlerFactory($handlerFactory);

        self::assertSame($handler, $generator->createHandler($filename));
    }

    /**
     * Tester of BasePath getter for getting default value
     *
     * @return void
     * @covers ::getBasePath
     */
    public function testGetDefaultBasePathDefault(): void
    {
        $this->processTestGetDefault(StaticContentGenerator::class, 'basePath', '');
    }

    /**
     * Tester of HandlerFactory default getter
     *
     * @return void
     * @covers ::getHandlerFactory
     * @covers ::setHandlerFactory
     */
    public function testGetDefaultHandlerFactory(): void
    {
        $outputBufferManager = $this->createOutputBufferManagerMock();
        $storage = $this->createStorageMock();
        $generator = $this->createGenerator($outputBufferManager, $storage);
        self::assertInstanceOf(StaticContentHandlerFactory::class, $generator->getHandlerFactory());
    }

    /**
     * Tester of StaticContentHandler getter/setter for missing handler
     *
     * @return void
     * @covers ::getHandler
     * @group negative
     */
    public function testGetSetStaticContentHandlerMissing(): void
    {
        $outputBufferManager = $this->createOutputBufferManagerMock();
        $storage = $this->createStorageMock();
        $generator = $this->createGenerator($outputBufferManager, $storage);
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('The :label expects to be :expected');
        $this->expectExceptionData(['expected' => 'initialized', 'label' => 'handler before get']);
        $generator->getHandler();
    }

    /**
     * Tester of BasePath getter/setter implementation
     *
     * @param string $basePath
     * @return void
     * @dataProvider stringDataProvider
     * @covers ::getBasePath
     * @covers ::setBasePath
     */
    public function testGetSetBasePath(string $basePath): void
    {
        $this->processTestGetSet(StaticContentGenerator::class, 'basePath', $basePath);
    }

    /**
     * Tester of BasePath setter for inserting unsupported value
     *
     * @param mixed $basePath
     * @phpstan-param AssertionExceptionData $data
     * @return void
     * @dataProvider unsupportedStringDataProvider
     * @covers ::setBasePath
     * @group negative
     */
    public function testSetBasePathUnsupportedValue(mixed $basePath, array $data): void
    {
        $this->processTestSetUnsupportedValue(StaticContentGenerator::class, 'basePath', $basePath, $data);
    }

    /**
     * Tester of BasePath getter/setter for work with trailing slash
     *
     * @return void
     * @covers ::getBasePath
     * @covers ::setBasePath
     */
    public function testGetSetBasePathWithTrailingSlash(): void
    {
        $value = 'path';
        $trailingValue = $value . '/';

        $outputBufferManager = $this->createOutputBufferManagerMock();
        $storage = $this->createStorageMock();
        $generator = $this->createGenerator($outputBufferManager, $storage);
        $generator->setBasePath($trailingValue);
        self::assertSame($value, $generator->getBasePath());
    }

    /**
     * Tester od StaticContentHandler implementation
     *
     * @return void
     * @covers ::getHandler
     * @covers ::setHandler
     */
    public function testGetSetHandler(): void
    {
        $handler = $this->createHandlerMock();
        $this->processTestGetSet(StaticContentGenerator::class, 'handler', $handler);
    }

    /**
     * Tester of StaticContentHandlerFactory getter/setter implementation
     *
     * @return void
     * @covers ::getHandlerFactory
     * @covers ::setHandlerFactory
     */
    public function testGetSetHandlerFactory(): void
    {
        $handlerFactory = $this->createHandlerFactoryMock();
        $this->processTestGetSet(StaticContentGenerator::class, 'handlerFactory', $handlerFactory);
    }

    /**
     * Tester od OutputBufferManager implementation
     *
     * @return void
     * @covers ::getOutputBufferManager
     * @covers ::setOutputBufferManager
     */
    public function testGetSetOutputBufferManager(): void
    {
        $outputBufferManager = $this->createOutputBufferManagerMock();
        $this->processTestGetSet(StaticContentGenerator::class, 'outputBufferManager', $outputBufferManager);
    }

    /**
     * Tester of Storage getter/setter implementation
     *
     * @return void
     * @covers ::getStorage
     * @covers ::setStorage
     */
    public function testGetSetStorage(): void
    {
        $storage = $this->createStorageMock();
        $this->processTestGetSet(StaticContentGenerator::class, 'storage', $storage);
    }

    /**
     * Tester of Begin processor
     *
     * @return void
     * @covers ::processBegin
     */
    public function testProcessBegin(): void
    {
        $handler = $this->createHandlerMock();
        $outputBufferManager = $this->createOutputBufferManagerMock(['beginHandlerService']);
        $outputBufferManager->expects(self::once())
            ->method('beginHandlerService')
            ->with(self::equalTo($handler));
        $storage = $this->createStorageMock();
        $generator = $this->createGenerator($outputBufferManager, $storage);
        $generator->setHandler($handler);

        $generator->processBegin();
    }

    /**
     * Tester of End processor
     *
     * @return void
     * @covers ::processEnd
     */
    public function testProcessEnd(): void
    {
        $handlerName = '$handlerName';
        $handler = $this->createHandlerMock(['getHandlerName']);
        $handler->expects(self::once())
            ->method('getHandlerName')
            ->willReturn($handlerName);
        $outputBufferManager = $this->createOutputBufferManagerMock(['end']);
        $outputBufferManager->expects(self::once())
            ->method('end')
            ->with(self::equalTo($handlerName));
        $storage = $this->createStorageMock();
        $generator = $this->createGenerator($outputBufferManager, $storage);
        $generator->setHandler($handler);

        $generator->processEnd();
        self::assertNull($this->getNonPublicPropertyValue($generator, 'handler'));
    }

    /**
     * Tester of End processor for stay StaticContentHandler
     *
     * @return void
     * @covers ::processEnd
     */
    public function testProcessEndStayStaticContentHandler(): void
    {
        $handlerName = '$handlerName';
        $handler = $this->createHandlerMock(['getHandlerName']);
        $handler->expects(self::once())
            ->method('getHandlerName')
            ->willReturn($handlerName);
        $outputBufferManager = $this->createOutputBufferManagerMock(['end']);
        $outputBufferManager->expects(self::once())
            ->method('end')
            ->with(self::equalTo($handlerName));
        $storage = $this->createStorageMock();
        $generator = $this->createGenerator($outputBufferManager, $storage);
        $generator->setHandler($handler);

        $generator->processEnd(false);
        self::assertSame($handler, $this->getNonPublicPropertyValue($generator, 'handler'));
    }

    /**
     * Request fileName data provider
     *
     * @phpstan-return Generator<string, array{0: string, 1: string}>
     */
    public function fileNameProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="">
        yield '' => ['', '/index.html'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="/">
        yield '/' => ['/', '/index.html'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="/index.html">
        yield '/index.html' => ['/index.html', '/index.html'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="/default.html">
        yield '/default.html' => ['/default.html', '/default.html'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="/test">
        yield '/test' => ['/test', '/test/index.html'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="/test/index.html">
        yield '/test/index.html' => ['/test/index.html', '/test/index.html'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="/test/test/">
        yield '/test/test/' => ['/test/test/', '/test/test/index.html'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="/test test/test test">
        yield '/test test/test test' => ['/test%20test/test%20test', '/test test/test test/index.html'];
        // </editor-fold>
    }

    /**
     * Tester of processRequestFileName implementation
     *
     * @param string $path
     * @param string $filename
     * @return void
     * @dataProvider fileNameProvider
     * @covers ::processFileName
     */
    public function testProcessRequestFileName(string $path, string $filename): void
    {
        $generator = $this->createGeneratorMock();
        self::assertSame($filename, $generator->processFileName($path));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * StaticContentGenerator factory
     *
     * @param OutputBufferManager $outputBufferManager
     * @param StorageInterface $storage
     * @return StaticContentGenerator
     */
    protected function createGenerator(
        OutputBufferManager $outputBufferManager,
        StorageInterface $storage
    ): StaticContentGenerator {
        $generator = new StaticContentGenerator($outputBufferManager, $storage);
        return $generator;
    }

    /**
     * StaticContentGenerator mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return StaticContentGenerator&MockObject
     */
    protected function createGeneratorMock(array $methods = []): StaticContentGenerator&MockObject
    {
        $mock = $this->createPartialMock(StaticContentGenerator::class, $methods);
        return $mock;
    }

    /**
     * StaticContentHandler mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return StaticContentHandler&MockObject
     */
    protected function createHandlerMock(array $methods = []): StaticContentHandler&MockObject
    {
        $mock = $this->createPartialMock(StaticContentHandler::class, $methods);
        return $mock;
    }

    /**
     * StaticContentHandlerFactory mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return StaticContentHandlerFactoryInterface&MockObject
     */
    protected function createHandlerFactoryMock(array $methods = []): StaticContentHandlerFactoryInterface&MockObject
    {
        $mock = $this->createMockAbstract(StaticContentHandlerFactoryInterface::class, $methods);
        return $mock;
    }

    /**
     * OutputBufferManager factory
     *
     * @return OutputBufferManager
     */
    protected function createOutputBufferManager(): OutputBufferManager
    {
        $outputBufferManager = new OutputBufferManager();
        return $outputBufferManager;
    }

    /**
     * OutputBufferManager mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return OutputBufferManager&MockObject
     */
    protected function createOutputBufferManagerMock(array $methods = []): OutputBufferManager&MockObject
    {
        $mock = $this->createPartialMock(OutputBufferManager::class, $methods);
        return $mock;
    }

    /**
     * Storage mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return StorageInterface&MockObject
     */
    protected function createStorageMock(array $methods = []): StorageInterface&MockObject
    {
        $mock = $this->createMockAbstract(StorageInterface::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
