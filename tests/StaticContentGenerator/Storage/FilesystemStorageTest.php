<?php

declare(strict_types=1);

namespace Interitty\StaticContentGenerator\Storage;

use Interitty\PhpUnit\BaseTestCase;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemWriter;
use Nette\IOException;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass Interitty\StaticContentGenerator\Storage\FilesystemStorage
 */
class FilesystemStorageTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of constructor
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstructor(): void
    {
        $filesystem = $this->createFilesystemMock();
        $filesystemStorage = $this->createFilesystemStorage($filesystem);
        self::assertSame($filesystem, $this->callNonPublicMethod($filesystemStorage, 'getFilesystem'));
    }

    /**
     * Tester of Filesystem getter/setter implementation
     *
     * @return void
     * @covers ::getFilesystem
     * @covers ::setFilesystem
     */
    public function testGetSetFilesystem(): void
    {
        $filesystem = $this->createFilesystemMock();
        $this->processTestGetSet(FilesystemStorage::class, 'filesystem', $filesystem);
    }

    /**
     * Tester of processPutContent
     *
     * @return void
     * @covers ::processPutContent
     */
    public function testProcessPutContent(): void
    {
        $content = 'content';
        $filename = 'filename';
        $filesystem = $this->createFilesystemMock();
        $filesystem->expects(self::once())
            ->method('write')
            ->with(self::equalTo($filename), self::equalTo($content));
        $filesystemStorage = $this->createFilesystemStorageMock(['getFilesystem']);
        $filesystemStorage->expects(self::once())
            ->method('getFilesystem')
            ->willReturn($filesystem);

        $filesystemStorage->processPutContent($filename, $content);
    }

    /**
     * Tester of processPutContent exception handler
     *
     * @return void
     * @covers ::processPutContent
     * @group negative
     */
    public function testProcessPutContentException(): void
    {
        $content = 'content';
        $exception = $this->createFilesystemExceptionStorageMock();
        $filename = 'filename';
        $filesystem = $this->createFilesystemMock();
        $filesystem->expects(self::once())
            ->method('write')
            ->with(self::equalTo($filename), self::equalTo($content))
            ->willThrowException($exception);
        $filesystemStorage = $this->createFilesystemStorageMock(['getFilesystem']);
        $filesystemStorage->expects(self::once())
            ->method('getFilesystem')
            ->willReturn($filesystem);

        $this->expectException(IOException::class);
        $this->expectExceptionMessage('Unable to write static content');
        $filesystemStorage->processPutContent($filename, $content);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * FilesystemException mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return FilesystemException&MockObject
     */
    protected function createFilesystemExceptionStorageMock(array $methods = []): FilesystemException&MockObject
    {
        $mock = $this->createPartialMock(FilesystemException::class, $methods);
        return $mock;
    }

    /**
     * Filesystem mock factory

     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return FilesystemWriter&MockObject
     */
    protected function createFilesystemMock(array $methods = []): FilesystemWriter&MockObject
    {
        $mock = $this->createMockAbstract(FilesystemWriter::class, $methods);
        return $mock;
    }

    /**
     * FilesystemStorage factory
     *
     * @param FilesystemWriter $filesystem
     * @return FilesystemStorage
     */
    protected function createFilesystemStorage(FilesystemWriter $filesystem): FilesystemStorage
    {
        $storage = new FilesystemStorage($filesystem);
        return $storage;
    }

    /**
     * FilesystemStorage mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return FilesystemStorage&MockObject
     */
    protected function createFilesystemStorageMock(array $methods = []): FilesystemStorage&MockObject
    {
        $mock = $this->createPartialMock(FilesystemStorage::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
