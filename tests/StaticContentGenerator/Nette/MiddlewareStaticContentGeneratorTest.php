<?php

declare(strict_types=1);

namespace Interitty\StaticContentGenerator\Nette;

use Generator;
use Interitty\OutputBufferManager\OutputBufferManager;
use Interitty\PhpUnit\BaseTestCase;
use Interitty\StaticContentGenerator\Handler\StaticContentHandler;
use Interitty\StaticContentGenerator\Storage\FilesystemStorage;
use Interitty\StaticContentGenerator\Storage\StorageInterface;
use Interitty\Utils\FileSystem as InterittyFileSystem;
use League\Flysystem\Filesystem;
use League\Flysystem\Local\LocalFilesystemAdapter;
use Nette\Http\IRequest as RequestInterface;
use Nette\Http\IResponse as ResponseInterface;
use Nette\Http\Request;
use Nette\Http\Response;
use Nette\Http\UrlScript;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;

use function array_merge;
use function sprintf;

use const LOCK_SH;

/**
 * @coversDefaultClass Interitty\StaticContentGenerator\Nette\MiddlewareStaticContentGenerator
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class MiddlewareStaticContentGeneratorTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of simple Integration of all the services
     *
     * @return void
     * @group integration
     * @covers ::processBegin
     * @covers ::processEnd
     */
    public function testSimpleIntegration(): void
    {
        $basePath = $this->createTempDirectory();
        $outputBufferManager = $this->createOutputBufferManager();
        $request = $this->createRequest('http://localhost/');
        $response = $this->createResponse(); //->setCode(ResponseInterface::S200_OK);
        $storage = $this->createFilesystemStorage($basePath);
        $generator = $this->createGenerator($request, $response, $outputBufferManager, $storage);
        $content = 'testContent';

        $generator->processBegin();
        echo $content;
        $generator->processEnd();

        $staticContentFile = $basePath . '/index.html';
        self::assertFileExists($staticContentFile);
        self::assertSame($content, InterittyFileSystem::read($staticContentFile));
    }

    /**
     * Tester of simple Integration of all the services on read-only production
     *
     * @return void
     * @covers ::processBegin
     * @group integration
     */
    public function testSimpleIntegrationReadOnly(): void
    {
        $basePath = $this->createTempDirectory();
        $outputBufferManager = $this->createOutputBufferManager();
        $request = $this->createRequest('http://localhost/');
        $response = $this->createResponse(); //->setCode(ResponseInterface::S200_OK);
        $storage = $this->createFilesystemStorage($basePath);
        $generator = $this->createGenerator($request, $response, $outputBufferManager, $storage);
        $generator->setReadOnly();
        $content = 'testContent';

        $generator->processBegin();
        echo $content;
        $generator->processEnd();

        $staticContentFile = $basePath . '/index.html';
        self::assertFileDoesNotExist($staticContentFile);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of constructor
     *
     * @return void
     * @covers ::__construct
     */
    public function testConstructor(): void
    {
        $outputBufferManager = $this->createOutputBufferManagerMock();
        $request = $this->createRequestMock();
        $response = $this->createResponseMock();
        $storage = $this->createStorageMock();

        $generator = $this->createGenerator($request, $response, $outputBufferManager, $storage);
        self::assertSame($request, $this->callNonPublicMethod($generator, 'getRequest'));
        self::assertSame($response, $this->callNonPublicMethod($generator, 'getResponse'));
        self::assertSame($outputBufferManager, $this->callNonPublicMethod($generator, 'getOutputBufferManager'));
        self::assertSame($storage, $this->callNonPublicMethod($generator, 'getStorage'));
    }

    /**
     * Tester of createRequestHandler factory
     *
     * @return void
     * @covers ::createRequestHandler
     */
    public function testCreateRequestHandler(): void
    {
        $fileName = 'fileName';
        $request = $this->createRequestMock();
        $handler = $this->createHandlerMock();
        $generator = $this->createGeneratorMock(['processRequestFileName', 'createHandler']);
        $generator->expects(self::once())
            ->method('processRequestFileName')
            ->with(self::equalTo($request))
            ->willReturn($fileName);
        $generator->expects(self::once())
            ->method('createHandler')
            ->with(self::equalTo($fileName))
            ->willReturn($handler);
        self::assertSame($handler, $generator->createRequestHandler($request));
    }

    /**
     * Tester of Logger getter/setter implementation
     *
     * @return void
     * @covers ::getLogger
     * @covers ::setLogger
     */
    public function testGetSetLogger(): void
    {
        $logger = $this->createLoggerMock();
        $this->processTestGetSet(MiddlewareStaticContentGenerator::class, 'logger', $logger);
    }

    /**
     * Tester of ReadOnly checker/setter implementation
     *
     * @return void
     * @covers ::isReadOnly
     * @covers ::setReadOnly
     */
    public function testGetCheckReadOnly(): void
    {
        $this->processTestGetSetBool(MiddlewareStaticContentGenerator::class, 'readOnly', true);
        $this->processTestGetSetBool(MiddlewareStaticContentGenerator::class, 'readOnly', false);
    }

    /**
     * Tester of Request getter/setter implementation
     *
     * @return void
     * @covers ::getRequest
     * @covers ::setRequest
     */
    public function testGetSetRequest(): void
    {
        $request = $this->createRequestMock();
        $this->processTestGetSet(MiddlewareStaticContentGenerator::class, 'request', $request);
    }

    /**
     * Tester of Response getter/setter implementation
     *
     * @return void
     * @covers ::getResponse
     * @covers ::setResponse
     */
    public function testGetSetResponse(): void
    {
        $response = $this->createResponseMock();
        $this->processTestGetSet(MiddlewareStaticContentGenerator::class, 'response', $response);
    }

    /**
     * Tester of processEnd for read-only production implementation
     *
     * @return void
     * @covers ::processEnd
     */
    public function testProcessEndReadOnly(): void
    {
        $generator = $this->createGeneratorMock([
            'isHandlerInProgress',
            'isReadOnly',
            'processIsAllowedBegin',
            'processIsWritable',
            'processReadOnlyWarning',
        ]);
        $generator->expects(self::once())->method('processIsWritable')->willReturn(true);
        $generator->expects(self::once())->method('isHandlerInProgress')->willReturn(false);
        $generator->expects(self::once())->method('isReadOnly')->willReturn(true);
        $generator->expects(self::once())->method('processIsAllowedBegin')->willReturn(true);
        $generator->expects(self::once())->method('processReadOnlyWarning');

        $generator->processEnd();
    }

    /**
     * Tester of processReadOnlyWarning implementation
     *
     * @return void
     * @covers ::processReadOnlyWarning
     */
    public function testProcessReadOnlyWarning(): void
    {
        $path = '/directory';
        $fileName = '/directory/index.html';
        $message = 'Static asset file "%s" for path "%s not exists on read-only filesystem';
        $logger = $this->createLoggerMock(['warning']);
        $logger->expects(self::once())->method('warning')->with(self::equalTo(sprintf($message, $fileName, $path)));
        $url = $this->createUrlScriptMock(['getPath']);
        $url->expects(self::once())->method('getPath')->willReturn($path);
        $request = $this->createRequestMock(['getUrl']);
        $request->expects(self::once())->method('getUrl')->willReturn($url);
        $generator = $this->createGeneratorMock(['getLogger', 'getRequest', 'processFileName']);
        $generator->expects(self::once())->method('getRequest')->willReturn($request);
        $generator->expects(self::once())->method('getLogger')->willReturn($logger);
        $generator->expects(self::once())->method('processFileName')
            ->with(self::equalTo($path))->willReturn($fileName);

        $this->callNonPublicMethod($generator, 'processReadOnlyWarning');
    }

    /**
     * ProcessIsAllowedBegin data provider
     *
     * @phpstan-return Generator<string, array{0: bool, 1: bool, 2: bool}>
     */
    public function isAllowedBeginProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="GET">
        yield 'GET' => [true, true, false];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="POST">
        yield 'POST' => [false, false, false];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Ajax">
        yield 'ajax' => [false, false, true];
        // </editor-fold>
    }

    /**
     * Tester of AllowedBegin checker processor
     *
     * @param bool $result
     * @param bool $isGetMethod
     * @param bool $isAjax
     * @return void
     * @dataProvider isAllowedBeginProvider
     * @covers ::processIsAllowedBegin
     */
    public function testProcessIsAllowedBegin(bool $result, bool $isGetMethod, bool $isAjax): void
    {
        $request = $this->createRequestMock(['isAjax', 'isMethod']);
        $request->expects(self::once())
            ->method('isMethod')
            ->with(self::equalTo(Request::Get))
            ->willReturn($isGetMethod);
        $request->expects(self::atMost(1))
            ->method('isAjax')
            ->willReturn($isAjax);
        $generator = $this->createGeneratorMock(['getRequest']);
        $generator->expects(self::once())
            ->method('getRequest')
            ->willReturn($request);
        self::assertSame($result, $this->callNonPublicMethod($generator, 'processIsAllowedBegin'));
    }

    /**
     * ProcessIsWritable data provider
     *
     * @phpstan-return Generator<string, array{0: bool, 1: int, 2: string|null, 3: string|null}>
     */
    public function isWritableProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="HTTP 200">
        yield 'HTTP 200' => [true, ResponseInterface::S200_OK, null, null];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="HTTP 404">
        yield 'HTTP 404' => [false, ResponseInterface::S404_NotFound, null, null];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Cache-Control: no-cache">
        yield 'Cache-Control: no-cache' => [false, ResponseInterface::S200_OK, 'no-cache', null];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Cache-Control: no-store">
        yield 'Cache-Control: no-store' => [false, ResponseInterface::S200_OK, 'no-store', null];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Pragma: no-cache">
        yield 'Pragma: no-cache' => [false, ResponseInterface::S200_OK, null, 'no-cache'];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Pragma: no-store">
        yield 'Pragma: no-store' => [false, ResponseInterface::S200_OK, null, 'no-store'];
        // </editor-fold>
    }

    /**
     * Tester of isWritable processor
     *
     * @param bool $result
     * @param int $code
     * @param string|null $cacheControl
     * @param string|null $pragma
     * @return void
     * @dataProvider isWritableProvider
     * @covers ::processIsWritable
     */
    public function testProcessIsWritable(bool $result, int $code, ?string $cacheControl, ?string $pragma): void
    {
        $response = $this->createResponseMock();
        $response->expects(self::once())
            ->method('getCode')
            ->willReturn($code);
        $response->expects(self::exactly(2))
            ->method('getHeader')
            ->willReturnCallback(static fn(string $name) => match ([$name]) { // @phpstan-ignore match.unhandled
                    ['Cache-Control'] => $cacheControl,
                    ['Pragma'] => $pragma,
            });
        $generator = $this->createGeneratorMock(['getResponse']);
        $generator->expects(self::once())
            ->method('getResponse')
            ->willReturn($response);
        self::assertSame($result, $this->callNonPublicMethod($generator, 'processIsWritable'));
    }

    /**
     * Tester of RequestFileName processor
     *
     * @return void
     * @covers ::processRequestFileName
     */
    public function testProcessRequestFileName(): void
    {
        $fileName = 'fileName';
        $path = 'path';
        $urlScript = $this->createUrlScriptMock(['getPath']);
        $urlScript->expects(self::once())
            ->method('getPath')
            ->willReturn($path);
        $request = $this->createRequestMock(['getUrl']);
        $request->expects(self::once())
            ->method('getUrl')
            ->willReturn($urlScript);
        $generator = $this->createGeneratorMock(['processFileName']);
        $generator->expects(self::once())
            ->method('processFileName')
            ->with(self::equalTo($path))
            ->willReturn($fileName);

        self::assertSame($fileName, $generator->processRequestFileName($request));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * FilesystemStorage factory
     *
     * @param string $basePath
     * @return FilesystemStorage
     */
    protected function createFilesystemStorage(string $basePath): FilesystemStorage
    {
        $filesystemAdapter = new LocalFilesystemAdapter($basePath, null, LOCK_SH);
        $filesystem = new Filesystem($filesystemAdapter);
        $storage = new FilesystemStorage($filesystem);
        return $storage;
    }

    /**
     * MiddlewareStaticContentGenerator factory
     *
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param OutputBufferManager $outputBufferManager
     * @param StorageInterface $storage
     * @return MiddlewareStaticContentGenerator
     */
    protected function createGenerator(
        RequestInterface $request,
        ResponseInterface $response,
        OutputBufferManager $outputBufferManager,
        StorageInterface $storage
    ): MiddlewareStaticContentGenerator {
        $generator = new MiddlewareStaticContentGenerator($request, $response, $outputBufferManager, $storage);
        return $generator;
    }

    /**
     * MiddlewareStaticContentGenerator mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return MiddlewareStaticContentGenerator&MockObject
     */
    protected function createGeneratorMock(
        array $methods = []
    ): MiddlewareStaticContentGenerator&MockObject {
        $mock = $this->createPartialMock(MiddlewareStaticContentGenerator::class, $methods);
        return $mock;
    }

    /**
     * StaticContentHandler mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return StaticContentHandler&MockObject
     */
    protected function createHandlerMock(array $methods = []): StaticContentHandler&MockObject
    {
        $mock = $this->createPartialMock(StaticContentHandler::class, $methods);
        return $mock;
    }

    /**
     * Logger mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return LoggerInterface&MockObject
     */
    protected function createLoggerMock(array $methods = []): LoggerInterface&MockObject
    {
        $mock = $this->createMockAbstract(LoggerInterface::class, $methods);
        return $mock;
    }

    /**
     * OutputBufferManager factory
     *
     * @return OutputBufferManager
     */
    protected function createOutputBufferManager(): OutputBufferManager
    {
        $outputBufferManager = new OutputBufferManager();
        return $outputBufferManager;
    }

    /**
     * OutputBufferManager mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return OutputBufferManager&MockObject
     */
    protected function createOutputBufferManagerMock(array $methods = []): OutputBufferManager&MockObject
    {
        $mock = $this->createPartialMock(OutputBufferManager::class, $methods);
        return $mock;
    }

    /**
     * Request factory
     *
     * @param string $url
     * @return Request
     */
    protected function createRequest(string $url): Request
    {
        $urlScript = new UrlScript($url);
        $request = new Request($urlScript);
        return $request;
    }

    /**
     * Request mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return Request&MockObject
     */
    protected function createRequestMock(array $methods = []): Request&MockObject
    {
        $mock = $this->createPartialMock(Request::class, $methods);
        return $mock;
    }

    /**
     * Response factory
     *
     * @return Response
     */
    protected function createResponse(): Response
    {
        $response = new Response();
        return $response;
    }

    /**
     * Response mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return ResponseInterface&MockObject
     */
    protected function createResponseMock(array $methods = []): ResponseInterface&MockObject
    {
        $mockMethods = array_merge($methods, [
            'deleteCookie',
            'redirect',
            'getCode',
            'setCode',
            'setCookie',
            'setContentType',
            'setExpiration',
            'addHeader',
            'getHeader',
            'setHeader',
            'getHeaders',
            'isSent',
        ]);
        $mock = $this->createPartialMock(ResponseInterface::class, $mockMethods);
        return $mock;
    }

    /**
     * Storage mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return FilesystemStorage&MockObject
     */
    protected function createStorageMock(array $methods = []): FilesystemStorage&MockObject
    {
        $mock = $this->createPartialMock(FilesystemStorage::class, $methods);
        return $mock;
    }

    /**
     * UrlScript mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return UrlScript&MockObject
     */
    protected function createUrlScriptMock(array $methods = []): UrlScript&MockObject
    {
        $mock = $this->createPartialMock(UrlScript::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
