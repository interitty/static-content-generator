<?php

declare(strict_types=1);

namespace Interitty\StaticContentGenerator\Nette\DI;

use Generator;
use Interitty\OutputBufferManager\OutputBufferManager;
use Interitty\PhpUnit\BaseIntegrationTestCase;
use Interitty\StaticContentGenerator\Handler\StaticContentHandlerFactoryInterface;
use Interitty\StaticContentGenerator\Nette\MiddlewareStaticContentGenerator;
use Interitty\StaticContentGenerator\Storage\FilesystemStorage;
use Interitty\Utils\Arrays;
use Interitty\Utils\FileSystem as InterittyFileSystem;
use League\Flysystem\Filesystem;
use Nette\Application\Application;
use Nette\DI\Container;
use Nette\DI\InvalidConfigurationException;
use Nette\Utils\AssertionException;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;

use function chmod;
use function ob_get_clean;
use function ob_start;
use function sprintf;

use const DIRECTORY_SEPARATOR;

/**
 * @coversDefaultClass Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class StaticContentGeneratorExtensionTest extends BaseIntegrationTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Dataprovider for processConfigUnsupported
     *
     * @phpstan-return Generator<string, array{0: string, 1: string}>
     */
    public function processConfigUnsupportedDataProvider(): Generator
    {
        // <editor-fold defaultstate="collapsed" desc="autoEnd">
        yield 'autoEnd' => [
            'autoEnd: foo',
            'The item \'staticContentGenerator › autoEnd\' expects to be bool, \'foo\' given.',
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="autoStart">
        yield 'autoStart' => [
            'autoStart: foo',
            'The item \'staticContentGenerator › autoStart\' expects to be bool, \'foo\' given.',
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="basePath">
        yield 'basePath' => [
            'basePath: false',
            'The item \'staticContentGenerator › basePath\' expects to be string, false given.',
        ];
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="destination">
        yield 'destination' => [
            'destination: false',
            'The item \'staticContentGenerator › destination\' expects to be string, false given.',
        ];
        // </editor-fold>
    }

    /**
     * Tester of Configuration loader for work with unsupported value
     *
     * @param string $neon
     * @param string $message
     * @return void
     * @dataProvider processConfigUnsupportedDataProvider
     * @covers ::getConfigSchema
     * @covers ::setupStaticContentGenerator
     * @group integration
     * @group negative
     */
    public function testProcessConfigUnsupported(string $neon, string $message): void
    {
        $configContent = '
extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension

staticContentGenerator:
    %s
';
        $configFile = $this->createTempFile(sprintf($configContent, $neon), 'config.neon');
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage($message);
        $this->createContainer($configFile);
    }

    /**
     * Tester of ApplicationProvider autoEnd setup
     *
     * @return void
     * @covers ::getConfigSchema
     * @covers ::setupApplicationProvider
     * @group integration
     */
    public function testSetupApplicationProviderAutoEnd(): void
    {
        $configContent = '
extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension

staticContentGenerator:
    autoEnd: true
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);
        $exception = $this->createAssertionExceptionMock();

        $generator = $this->createStaticContentGeneratorMock(['processEnd']);
        $generator->expects(self::once())
            ->method('processEnd');
        $container->removeService('staticContentGenerator.staticContentGenerator');
        $container->addService('staticContentGenerator.staticContentGenerator', $generator);

        /** @var Application $application */
        $application = $container->getByType(Application::class);
        self::assertCount(0, $application->onStartup);
        self::assertCount(1, $application->onShutdown);

        Arrays::invoke($application->onShutdown, $application);
        Arrays::invoke($application->onShutdown, $application, $exception);
    }

    /**
     * Tester of ApplicationProvider autoStart setup
     *
     * @return void
     * @covers ::getConfigSchema
     * @covers ::setupApplicationProvider
     * @group integration
     */
    public function testSetupApplicationProviderAutoStart(): void
    {
        $configContent = '
extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension

staticContentGenerator:
    autoStart: true
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);

        $generator = $this->createStaticContentGeneratorMock(['processBegin']);
        $generator->expects(self::once())
            ->method('processBegin');
        $container->removeService('staticContentGenerator.staticContentGenerator');
        $container->addService('staticContentGenerator.staticContentGenerator', $generator);

        /** @var Application $application */
        $application = $container->getByType(Application::class);
        self::assertCount(1, $application->onStartup);
        self::assertCount(1, $application->onShutdown);

        Arrays::invoke($application->onStartup, $application);
    }

    /**
     * Tester of Filesystem setup
     *
     * @return void
     * @covers ::setupFilesystem
     * @group integration
     */
    public function testSetupFilesystem(): void
    {
        $configContent = '
extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);

        $serviceName = 'staticContentGenerator.' . StaticContentGeneratorExtension::PROVIDER_FILESYSTEM;
        self::assertTrue($container->hasService($serviceName));
        self::assertInstanceOf(Filesystem::class, $container->getService($serviceName));
    }

    /**
     * Tester of Filesystem setup alias
     *
     * @return void
     * @covers ::setupFilesystem
     * @group integration
     */
    public function testSetupFilesystemAlias(): void
    {
        $configContent = '
services:
    ownFilesystemAdapter:
        factory: League\Flysystem\Local\LocalFilesystemAdapter(%wwwDir%)

    ownFilesystem:
        factory: League\Flysystem\Filesystem(@ownFilesystemAdapter)

extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);
        $aliases = $this->getNonPublicPropertyValue($container, 'aliases');
        $serviceName = 'staticContentGenerator.' . StaticContentGeneratorExtension::PROVIDER_FILESYSTEM;
        self::assertTrue($container->hasService($serviceName));
        self::assertSame('ownFilesystem', $aliases[$serviceName]);
    }

    /**
     * Tester of OutputBufferManager setup
     *
     * @return void
     * @covers ::setupOutputBufferManager
     * @group integration
     */
    public function testSetupOutputBufferManager(): void
    {
        $configContent = '
extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);

        $serviceName = 'staticContentGenerator.' . StaticContentGeneratorExtension::PROVIDER_OUTPUT_BUFFER_MANAGER;
        self::assertTrue($container->hasService($serviceName));
        self::assertInstanceOf(OutputBufferManager::class, $container->getService($serviceName));
    }

    /**
     * Tester of OutputBufferManager setup alias
     *
     * @return void
     * @covers ::setupOutputBufferManager
     * @group integration
     */
    public function testSetupOutputBufferManagerAlias(): void
    {
        $configContent = '
services:
    ownOutputBufferManager:
        factory: Interitty\OutputBufferManager\OutputBufferManager

extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);
        $aliases = $this->getNonPublicPropertyValue($container, 'aliases');
        $serviceName = 'staticContentGenerator.' . StaticContentGeneratorExtension::PROVIDER_OUTPUT_BUFFER_MANAGER;
        self::assertTrue($container->hasService($serviceName));
        self::assertSame('ownOutputBufferManager', $aliases[$serviceName]);
    }

    /**
     * Tester of StaticContentHandlerFactory setup
     *
     * @return void
     * @covers ::setupHandlerFactory
     * @group integration
     */
    public function testSetupStaticContentHandlerFactory(): void
    {
        $configContent = '
extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);

        $serviceName = 'staticContentGenerator.' . StaticContentGeneratorExtension::PROVIDER_SETUP_HANDLER_FACTORY;
        self::assertTrue($container->hasService($serviceName));
        self::assertInstanceOf(StaticContentHandlerFactoryInterface::class, $container->getService($serviceName));
    }

    /**
     * Tester of Storage setup
     *
     * @return void
     * @covers ::setupStorage
     * @group integration
     */
    public function testSetupStorage(): void
    {
        $configContent = '
extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);

        $serviceName = 'staticContentGenerator.' . StaticContentGeneratorExtension::PROVIDER_STORAGE;
        self::assertTrue($container->hasService($serviceName));
        self::assertInstanceOf(FilesystemStorage::class, $container->getService($serviceName));
    }

    /**
     * Tester of Storage setup alias
     *
     * @return void
     * @covers ::setupStorage
     * @group integration
     */
    public function testSetupStorageAlias(): void
    {
        $configContent = '
services:
    ownFilesystemStorage:
        factory: Interitty\StaticContentGenerator\Storage\FilesystemStorage

extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);
        $aliases = $this->getNonPublicPropertyValue($container, 'aliases');
        $serviceName = 'staticContentGenerator.' . StaticContentGeneratorExtension::PROVIDER_STORAGE;
        self::assertTrue($container->hasService($serviceName));
        self::assertSame('ownFilesystemStorage', $aliases[$serviceName]);
    }

    /**
     * Tester of simple Integration all services
     *
     * @return void
     * @covers ::getConfigSchema
     * @covers ::setupStaticContentGenerator
     * @group integration
     */
    public function testSimpleIntegration(): void
    {
        $configContent = '
extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension
staticContentGenerator:
    autoStart: true
    basePath: /
    destination: %s
    writeFlags: ::constant(LOCK_SH)
';

        $wwwDirectory = $this->createTempDirectory('www');
        $configFile = $this->createTempFile(sprintf($configContent, $wwwDirectory), 'config.neon');
        $content = 'Test';
        $staticFilePath = $wwwDirectory . DIRECTORY_SEPARATOR . 'index.html';

        ob_start();
        $container = $this->createIntegrationContainer($configFile, $content);
        /** @var Application $application */
        $application = $container->getByType(Application::class);
        $application->run();
        self::assertSame($content, ob_get_clean());
        self::assertFileExists($staticFilePath);
        self::assertSame($content, InterittyFileSystem::read($staticFilePath));
    }

    /**
     * Tester of simple Integration of all services for read-only enabled mode
     *
     * @return void
     * @covers ::getConfigSchema
     * @covers ::setupStaticContentGenerator
     * @group integration
     */
    public function testSimpleIntegrationReadonly(): void
    {
        $configContent = '
extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension
staticContentGenerator:
    destination: %s
    readOnly: true
';

        $wwwDirectory = $this->createTempDirectory('www');
        $configFile = $this->createTempFile(sprintf($configContent, $wwwDirectory), 'config.neon');
        $content = 'Test';
        $staticFilePath = $wwwDirectory . DIRECTORY_SEPARATOR . 'index.html';

        ob_start();
        $container = $this->createIntegrationContainer($configFile, $content);
        /** @var Application $application */
        $application = $container->getByType(Application::class);
        $application->run();
        /** @var MiddlewareStaticContentGenerator $generator */
        $generator = $container->getByType(MiddlewareStaticContentGenerator::class);
        self::assertSame($content, ob_get_clean());
        self::assertTrue($generator->isReadOnly());
        self::assertFileDoesNotExist($staticFilePath);
    }

    /**
     * Tester of simple Integration of all services for read-only detection mode
     *
     * @return void
     * @covers ::getConfigSchema
     * @covers ::setupStaticContentGenerator
     * @group integration
     */
    public function testSimpleIntegrationReadonlyDetect(): void
    {
        $configContent = '
extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension
staticContentGenerator:
    destination: %s
';

        $wwwDirectory = $this->createTempDirectory('www');
        chmod($wwwDirectory, 0000);
        $configFile = $this->createTempFile(sprintf($configContent, $wwwDirectory), 'config.neon');
        $content = 'Test';
        $staticFilePath = $wwwDirectory . DIRECTORY_SEPARATOR . 'index.html';

        ob_start();
        $container = $this->createIntegrationContainer($configFile, $content);
        /** @var Application $application */
        $application = $container->getByType(Application::class);
        $application->run();
        /** @var MiddlewareStaticContentGenerator $generator */
        $generator = $container->getByType(MiddlewareStaticContentGenerator::class);
        self::assertSame($content, ob_get_clean());
        self::assertTrue($generator->isReadOnly());
        self::assertFileDoesNotExist($staticFilePath);
    }

    /**
     * Tester of Static content generator setup
     *
     * @return void
     * @covers ::getConfigSchema
     * @covers ::setupStaticContentGenerator
     * @group integration
     */
    public function testSetupStaticContentGenerator(): void
    {
        $configContent = '
extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension

staticContentGenerator:
    basePath: /
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);

        /** @var MiddlewareStaticContentGenerator $generator */
        $generator = $container->getByType(MiddlewareStaticContentGenerator::class);
        self::assertSame('', $generator->getBasePath());
    }

    /**
     * Tester of Static content generator setup for expanded parameters
     *
     * @return void
     * @covers ::getConfigSchema
     * @covers ::setupStaticContentGenerator
     * @group integration
     */
    public function testSetupStaticContentGeneratorExpanded(): void
    {
        $configContent = '
parameters:
    staticContentBasePath: /public

extensions:
    staticContentGenerator: Interitty\StaticContentGenerator\Nette\DI\StaticContentGeneratorExtension

staticContentGenerator:
    basePath: %staticContentBasePath%
';
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $container = $this->createContainer($configFile);

        /** @var MiddlewareStaticContentGenerator $generator */
        $generator = $container->getByType(MiddlewareStaticContentGenerator::class);
        self::assertSame('public', $generator->getBasePath());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of beforeCompile
     *
     * @return void
     * @covers ::beforeCompile
     */
    public function testBeforeCompile(): void
    {
        $extension = $this->createStaticContentGeneratorExtensionMock([
            'setupFilesystem',
            'setupOutputBufferManager',
            'setupStorage',
            'setupHandlerFactory',
            'setupStaticContentGenerator',
            'setupApplicationProvider',
        ]);
        $extension->expects(self::once())->method('setupFilesystem');
        $extension->expects(self::once())->method('setupOutputBufferManager');
        $extension->expects(self::once())->method('setupStorage');
        $extension->expects(self::once())->method('setupHandlerFactory');
        $extension->expects(self::once())->method('setupStaticContentGenerator');
        $extension->expects(self::once())->method('setupApplicationProvider');
        $extension->beforeCompile();
    }

    /**
     * Tester of defaults getter implementation
     *
     * @return void
     * @covers ::getDefaults
     */
    public function testGetDefaults(): void
    {
        $extension = $this->createStaticContentGeneratorExtensionMock();
        $defaults = $this->callNonPublicMethod($extension, 'getDefaults');
        self::assertArrayHasKey(StaticContentGeneratorExtension::CONFIG_AUTO_END, $defaults);
        self::assertArrayHasKey(StaticContentGeneratorExtension::CONFIG_AUTO_START, $defaults);
        self::assertArrayHasKey(StaticContentGeneratorExtension::CONFIG_BASE_PATH, $defaults);
        self::assertArrayHasKey(StaticContentGeneratorExtension::CONFIG_DESTINATION, $defaults);
        self::assertArrayHasKey(StaticContentGeneratorExtension::CONFIG_WRITE_FLAGS, $defaults);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Integration container factory
     *
     * @param string $configFile
     * @param string $content
     * @return Container
     */
    protected function createIntegrationContainer(string $configFile, string $content): Container
    {
        $configContent = '
services:
    http.request: Nette\Http\Request(@urlScript)
    routing.router:
        factory: Nette\Application\Routers\RouteList
        setup:
            - addRoute(\'/\', [
                \'presenter\': \'Nette:Micro\'
                \'callback\': \Closure::fromCallable([@container, \'createServiceTextResponse\'])
              ])
    textResponse: Nette\Application\Responses\TextResponse(\'%s\')
    urlScript: Nette\Http\UrlScript(\'http://foo.bar/\')
';
        $contentConfigFile = $this->createTempFile(sprintf($configContent, $content), 'content.neon');
        $configurator = $this->createConfigurator($configFile);
        $configurator->addConfig($contentConfigFile);
        $container = $configurator->createContainer();
        return $container;
    }

    /**
     * AssertionException mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return AssertionException&MockObject
     */
    protected function createAssertionExceptionMock(array $methods = []): AssertionException&MockObject
    {
        $mock = $this->createPartialMock(AssertionException::class, $methods);
        return $mock;
    }

    /**
     * Logger mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return LoggerInterface&MockObject
     */
    protected function createLoggerMock(array $methods = []): LoggerInterface&MockObject
    {
        $mock = $this->createMockAbstract(LoggerInterface::class, $methods);
        return $mock;
    }

    /**
     * StaticContentGenerator mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return MiddlewareStaticContentGenerator&MockObject
     */
    protected function createStaticContentGeneratorMock(
        array $methods = []
    ): MiddlewareStaticContentGenerator&MockObject {
        $mock = $this->createPartialMock(MiddlewareStaticContentGenerator::class, $methods);
        return $mock;
    }

    /**
     * StaticContentGeneratorExtension mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return StaticContentGeneratorExtension&MockObject
     */
    protected function createStaticContentGeneratorExtensionMock(
        array $methods = []
    ): StaticContentGeneratorExtension&MockObject {
        $mock = $this->createPartialMock(StaticContentGeneratorExtension::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
