<?php

declare(strict_types=1);

namespace Interitty\StaticContentGenerator\Nette;

use Interitty\OutputBufferManager\OutputBufferManager;
use Interitty\StaticContentGenerator\Handler\StaticContentHandler;
use Interitty\StaticContentGenerator\StaticContentGenerator;
use Interitty\StaticContentGenerator\Storage\StorageInterface;
use Nette\Http\IRequest as RequestInterface;
use Nette\Http\IResponse as ResponseInterface;
use Psr\Log\LoggerInterface;

use function sprintf;
use function stripos;

class MiddlewareStaticContentGenerator extends StaticContentGenerator
{
    /** @var LoggerInterface|null */
    protected ?LoggerInterface $logger;

    /** @var RequestInterface */
    protected RequestInterface $request;

    /** @var ResponseInterface */
    protected ResponseInterface $response;

    /** @var bool */
    protected bool $readOnly = false;

    /**
     * Constructor
     *
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param OutputBufferManager $outputBufferManager
     * @param StorageInterface $storage
     * @param LoggerInterface|null $logger
     * @return void
     */
    public function __construct(
        RequestInterface $request,
        ResponseInterface $response,
        OutputBufferManager $outputBufferManager,
        StorageInterface $storage,
        ?LoggerInterface $logger = null
    ) {
        parent::__construct($outputBufferManager, $storage);
        $this->setRequest($request);
        $this->setResponse($response);
        $this->setLogger($logger);
    }

    /**
     * Begin processor
     *
     * @return void
     */
    public function processBegin(): void
    {
        if (($this->processIsAllowedBegin() === true) && ($this->isReadOnly() === false)) {
            $request = $this->getRequest();
            $handler = $this->createRequestHandler($request);
            $this->setHandler($handler);
            parent::processBegin();
        }
    }

    /**
     * End processor
     *
     * @param bool $clearStaticContent
     * @return void
     */
    public function processEnd(bool $clearStaticContent = true): void
    {
        if ($this->processIsWritable() === true) {
            if ($this->isHandlerInProgress() === true) {
                parent::processEnd($clearStaticContent);
            } elseif (($this->isReadOnly() === true) && ($this->processIsAllowedBegin() === true)) {
                $this->processReadOnlyWarning();
            }
        }
    }

    /**
     * Read-only filesystem warning processor
     *
     * @return void
     */
    protected function processReadOnlyWarning(): void
    {
        $path = $this->getRequest()->getUrl()->getPath();
        $fileName = $this->processFileName($path);
        $message = 'Static asset file "%s" for path "%s not exists on read-only filesystem';
        $this->getLogger()?->warning(sprintf($message, $fileName, $path));
    }

    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Handler by given request factory
     *
     * @param RequestInterface $request
     * @return StaticContentHandler
     */
    public function createRequestHandler(RequestInterface $request): StaticContentHandler
    {
        $fileName = $this->processRequestFileName($request);
        $handler = $this->createHandler($fileName);
        return $handler;
    }

    /**
     * Allowed begin checker processor
     *
     * @return bool
     */
    protected function processIsAllowedBegin(): bool
    {
        $isAllowedBegin = true;
        $request = $this->getRequest();
        switch (true) {
            case ($request->isMethod(RequestInterface::Get) !== true):
            case ($request->isAjax() !== false):
                $isAllowedBegin = false;
        }
        return $isAllowedBegin;
    }

    /**
     * Writable checker processor
     *
     * @return bool
     */
    protected function processIsWritable()
    {
        $isWritable = true;
        $response = $this->getResponse();
        $cacheControl = (string) $response->getHeader('Cache-Control');
        $pragma = (string) $response->getHeader('Pragma');
        switch (true) {
            case ($response->getCode() !== ResponseInterface::S200_OK):
            case ((stripos($cacheControl, 'no-cache') !== false) || (stripos($cacheControl, 'no-store') !== false)):
            case ((stripos($pragma, 'no-cache') !== false) || (stripos($pragma, 'no-store') !== false)):
                $isWritable = false;
        }
        return $isWritable;
    }

    /**
     * FileName from given Request processor
     *
     * @param RequestInterface $request
     * @return string
     */
    public function processRequestFileName(RequestInterface $request): string
    {
        $path = $request->getUrl()->getPath();
        $fileName = $this->processFileName($path);
        return $fileName;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Logger getter
     *
     * @return LoggerInterface|null
     */
    protected function getLogger(): ?LoggerInterface
    {
        return $this->logger;
    }

    /**
     * Logger setter
     *
     * @param LoggerInterface|null $logger
     * @return static Provides fluent interface
     */
    protected function setLogger(?LoggerInterface $logger): static
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * ReadOnly checker
     *
     * @return bool
     */
    public function isReadOnly(): bool
    {
        return $this->readOnly;
    }

    /**
     * ReadOnly setter
     *
     * @param bool $readOnly
     * @return static Provides fluent interface
     */
    public function setReadOnly(bool $readOnly = true): static
    {
        $this->readOnly = $readOnly;
        return $this;
    }

    /**
     * Request getter
     *
     * @return RequestInterface
     */
    protected function getRequest(): RequestInterface
    {
        return $this->request;
    }

    /**
     * Request setter
     *
     * @param RequestInterface $request
     * @return static Provides fluent interface
     */
    protected function setRequest(RequestInterface $request): static
    {
        $this->request = $request;
        return $this;
    }

    /**
     * Response getter
     *
     * @return ResponseInterface
     */
    protected function getResponse(): ResponseInterface
    {
        return $this->response;
    }

    /**
     * Response setter
     *
     * @param ResponseInterface $response
     * @return static Provides fluent interface
     */
    protected function setResponse(ResponseInterface $response): static
    {
        $this->response = $response;
        return $this;
    }

    // </editor-fold>
}
