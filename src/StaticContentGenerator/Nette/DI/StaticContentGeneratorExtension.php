<?php

declare(strict_types=1);

namespace Interitty\StaticContentGenerator\Nette\DI;

use Interitty\DI\CompilerExtension;
use Interitty\OutputBufferManager\OutputBufferManager;
use Interitty\StaticContentGenerator\Handler\StaticContentHandlerFactoryInterface;
use Interitty\StaticContentGenerator\Nette\MiddlewareStaticContentGenerator;
use Interitty\StaticContentGenerator\Storage\FilesystemStorage;
use Interitty\StaticContentGenerator\Storage\StorageInterface;
use League\Flysystem\Filesystem;
use League\Flysystem\Local\LocalFilesystemAdapter;
use Nette\Schema\Expect;
use Nette\Schema\Schema;

use const LOCK_EX;

class StaticContentGeneratorExtension extends CompilerExtension
{
    /** All config parameter name constants */
    public const string CONFIG_AUTO_END = 'autoEnd';
    public const string CONFIG_AUTO_START = 'autoStart';
    public const string CONFIG_BASE_PATH = 'basePath';
    public const string CONFIG_DESTINATION = 'destination';
    public const string CONFIG_PRODUCTION_MODE = 'productionMode';
    public const string CONFIG_READ_ONLY = 'readOnly';
    public const string CONFIG_WRITE_FLAGS = 'writeFlags';

    /** All available provider name constants */
    public const string PROVIDER_FILESYSTEM = 'filesystem';
    public const string PROVIDER_FILESYSTEM_ADAPTER = 'filesystemAdapter';
    public const string PROVIDER_OUTPUT_BUFFER_MANAGER = 'outputBufferManager';
    public const string PROVIDER_SETUP_HANDLER_FACTORY = 'setupHandlerFactory';
    public const string PROVIDER_STATIC_CONTENT_GENERATOR = 'staticContentGenerator';
    public const string PROVIDER_STORAGE = 'storage';

    /**
     * @var mixed[]
     */
    protected $defaults = [
        self::CONFIG_AUTO_END => false,
        self::CONFIG_AUTO_START => false,
        self::CONFIG_BASE_PATH => '/',
        self::CONFIG_DESTINATION => '%wwwDir%',
        self::CONFIG_PRODUCTION_MODE => '%productionMode%',
        self::CONFIG_READ_ONLY => null,
        self::CONFIG_WRITE_FLAGS => LOCK_EX,
    ];

    /**
     * @inheritdoc
     */
    public function beforeCompile(): void
    {
        $this->setupFilesystem();
        $this->setupOutputBufferManager();
        $this->setupStorage();
        $this->setupHandlerFactory();
        $this->setupStaticContentGenerator();
        $this->setupApplicationProvider();
    }

    /**
     * @inheritdoc
     */
    public function getConfigSchema(): Schema
    {
        parent::getConfigSchema();
        $defaults = $this->getDefaults();
        return Expect::structure([
                self::CONFIG_AUTO_END => Expect::bool()->default($defaults[self::CONFIG_AUTO_END]),
                self::CONFIG_AUTO_START => Expect::bool()->default($defaults[self::CONFIG_AUTO_START]),
                self::CONFIG_BASE_PATH => Expect::string()->default($defaults[self::CONFIG_BASE_PATH]),
                self::CONFIG_DESTINATION => Expect::string()->default($defaults[self::CONFIG_DESTINATION]),
                self::CONFIG_PRODUCTION_MODE => Expect::bool()->default($defaults[self::CONFIG_PRODUCTION_MODE]),
                self::CONFIG_READ_ONLY => Expect::bool()->nullable()->default($defaults[self::CONFIG_READ_ONLY]),
                self::CONFIG_WRITE_FLAGS => Expect::mixed()->default($defaults[self::CONFIG_WRITE_FLAGS]),
        ]);
    }

    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Application provider setup helper
     *
     * @return void
     */
    protected function setupApplicationProvider(): void
    {
        $configAutoStart = $this->getConfigField(self::CONFIG_AUTO_START);
        $application = $this->getServiceDefinition($this->getContainerBuilder(), 'application.application');
        if ($configAutoStart === true) {
            $startupSetupString = '?->onStartup[] = function () {?->processBegin();}';
            $application->addSetup($startupSetupString, [
                '@self',
                $this->prefix('@' . self::PROVIDER_STATIC_CONTENT_GENERATOR),
            ]);
        }
        if (($configAutoStart === true) || ($this->getConfigField(self::CONFIG_AUTO_END) === true)) {
            $shutdownSetupString = '?->onShutdown[] = function ($application, $exception = null) {'
                . '    if($exception === null) {'
                . '        ?->processEnd();'
                . '    }'
                . '}';
            $application->addSetup($shutdownSetupString, [
                '@self',
                $this->prefix('@' . self::PROVIDER_STATIC_CONTENT_GENERATOR),
            ]);
        }
    }

    /**
     * Filesystem setup helper
     *
     * @return void
     */
    protected function setupFilesystem(): void
    {
        if ($this->setupServiceAlias(Filesystem::class, self::PROVIDER_FILESYSTEM) === false) {
            $builder = $this->getContainerBuilder();
            $arguments = [
                $this->getConfigField(self::CONFIG_DESTINATION),
                null,
                $this->getConfigField(self::CONFIG_WRITE_FLAGS),
            ];
            $builder->addDefinition($this->prefix(self::PROVIDER_FILESYSTEM_ADAPTER))
                ->setFactory(LocalFilesystemAdapter::class)
                ->setArguments($arguments);

            $builder->addDefinition($this->prefix(self::PROVIDER_FILESYSTEM))
                ->setFactory(Filesystem::class)
                ->setArguments([$this->prefix('@' . self::PROVIDER_FILESYSTEM_ADAPTER)]);
        }
    }

    /**
     * Handler factory setup helper
     *
     * @return void
     */
    protected function setupHandlerFactory(): void
    {
        $builder = $this->getContainerBuilder();
        $builder->addFactoryDefinition($this->prefix(self::PROVIDER_SETUP_HANDLER_FACTORY))
            ->setImplement(StaticContentHandlerFactoryInterface::class);
    }

    /**
     * Output buffer manager setup helper
     *
     * @return void
     */
    protected function setupOutputBufferManager(): void
    {
        if ($this->setupServiceAlias(OutputBufferManager::class, self::PROVIDER_OUTPUT_BUFFER_MANAGER) === false) {
            $builder = $this->getContainerBuilder();
            $builder->addDefinition($this->prefix(self::PROVIDER_OUTPUT_BUFFER_MANAGER))
                ->setFactory(OutputBufferManager::class);
        }
    }

    /**
     * Static content generator setup helper
     *
     * @return void
     */
    protected function setupStaticContentGenerator(): void
    {
        $readOnly = $this->getConfigField(self::CONFIG_READ_ONLY);
        $builder = $this->getContainerBuilder();
        $definition = $builder->addDefinition($this->prefix(self::PROVIDER_STATIC_CONTENT_GENERATOR))
            ->setFactory(MiddlewareStaticContentGenerator::class)
            ->setArguments([
                '@http.request',
                '@http.response',
                $this->prefix('@' . self::PROVIDER_OUTPUT_BUFFER_MANAGER),
                $this->prefix('@' . self::PROVIDER_STORAGE),
            ])
            ->addSetup('setHandlerFactory', [
                $this->prefix('@' . self::PROVIDER_SETUP_HANDLER_FACTORY),
            ])
            ->addSetup('setBasePath', [$this->getConfigField(self::CONFIG_BASE_PATH)]);

        if ($readOnly !== null) {
            $definition->addSetup('$service->setReadOnly(?)', [$readOnly]);
        } elseif ($this->getConfigField(self::CONFIG_PRODUCTION_MODE) === true) {
            $destination = $this->getConfigField(self::CONFIG_DESTINATION);
            $definition->addSetup('$service->setReadOnly(is_writable(?) === false)', [$destination]);
        }
    }

    /**
     * Storage setup helper
     *
     * @return void
     */
    protected function setupStorage(): void
    {
        if ($this->setupServiceAlias(StorageInterface::class, self::PROVIDER_STORAGE) === false) {
            $builder = $this->getContainerBuilder();
            $builder->addDefinition($this->prefix(self::PROVIDER_STORAGE))
                ->setFactory(FilesystemStorage::class);
        }
    }

    // </editor-fold>
}
