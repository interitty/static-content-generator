<?php

declare(strict_types=1);

namespace Interitty\StaticContentGenerator\Storage;

use Interitty\Exceptions\Exceptions;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemWriter;
use Nette\IOException;

class FilesystemStorage implements StorageInterface
{
    /** @var string */
    protected string $filename;

    /** @var FilesystemWriter */
    protected FilesystemWriter $filesystem;

    /**
     * Constructor
     *
     * @param FilesystemWriter $filesystem
     * @return void
     */
    public function __construct(FilesystemWriter $filesystem)
    {
        $this->setFilesystem($filesystem);
    }

    /**
     * @inheritdoc
     */
    public function processPutContent(string $filename, string $content): void
    {
        try {
            $this->getFilesystem()->write($filename, $content);
        } catch (FilesystemException $exception) {
            throw Exceptions::extend(IOException::class)
                    ->setMessage('Unable to write static content')
                    ->setPrevious($exception);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Filesystem getter
     *
     * @return FilesystemWriter
     */
    protected function getFilesystem(): FilesystemWriter
    {
        return $this->filesystem;
    }

    /**
     * Filesystem setter
     *
     * @param FilesystemWriter $filesystem
     * @return static Provides fluent interface
     */
    protected function setFilesystem(FilesystemWriter $filesystem): static
    {
        $this->filesystem = $filesystem;
        return $this;
    }

    // </editor-fold>
}
