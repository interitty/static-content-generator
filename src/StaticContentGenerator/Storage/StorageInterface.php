<?php

declare(strict_types=1);

namespace Interitty\StaticContentGenerator\Storage;

interface StorageInterface
{
    /**
     * Put content processor
     *
     * @param string $filename
     * @param string $content
     * @return void
     */
    public function processPutContent(string $filename, string $content): void;
}
