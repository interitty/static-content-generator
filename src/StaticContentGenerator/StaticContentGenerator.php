<?php

declare(strict_types=1);

namespace Interitty\StaticContentGenerator;

use Interitty\OutputBufferManager\OutputBufferManager;
use Interitty\StaticContentGenerator\Handler\StaticContentHandler;
use Interitty\StaticContentGenerator\Handler\StaticContentHandlerFactory;
use Interitty\StaticContentGenerator\Handler\StaticContentHandlerFactoryInterface;
use Interitty\StaticContentGenerator\Storage\StorageInterface;
use Interitty\Utils\Strings;
use Interitty\Utils\Validators;

use function assert;
use function in_array;
use function ltrim;
use function pathinfo;
use function rtrim;
use function urldecode;

class StaticContentGenerator
{
    /** @var string */
    protected string $basePath = '';

    /** @var StaticContentHandler|null */
    protected ?StaticContentHandler $handler = null;

    /** @var StaticContentHandlerFactoryInterface */
    protected StaticContentHandlerFactoryInterface $handlerFactory;

    /** @var OutputBufferManager */
    protected OutputBufferManager $outputBufferManager;

    /** @var StorageInterface */
    protected StorageInterface $storage;

    /**
     * Constructor
     *
     * @param OutputBufferManager $outputBufferManager
     * @param StorageInterface $storage
     * @return void
     */
    public function __construct(OutputBufferManager $outputBufferManager, StorageInterface $storage)
    {
        $this->setOutputBufferManager($outputBufferManager);
        $this->setStorage($storage);
    }

    /**
     * Begin processor
     *
     * @return void
     */
    public function processBegin(): void
    {
        $handler = $this->getHandler();
        $this->getOutputBufferManager()->beginHandlerService($handler);
    }

    /**
     * End processor
     *
     * @param bool $clearStaticContent [OPTIONAL]
     * @return void
     */
    public function processEnd(bool $clearStaticContent = true): void
    {
        $handler = $this->getHandler();
        $this->getOutputBufferManager()->end($handler->getHandlerName());
        if ($clearStaticContent === true) {
            $this->handler = null;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Handler factory
     *
     * @param string $filename
     * @param bool $muteOutput [OPTIONAL]
     * @return StaticContentHandler
     */
    public function createHandler(string $filename, bool $muteOutput = false): StaticContentHandler
    {
        $storage = $this->getStorage();
        $handler = $this->getHandlerFactory()->create($storage, $filename, $muteOutput);
        return $handler;
    }

    /**
     * Handler in progress checker
     *
     * @return bool
     */
    public function isHandlerInProgress(): bool
    {
        $inProgress = false;
        if ($this->handler instanceof StaticContentHandler) {
            $handler = $this->getHandler();
            $inProgress = $this->getOutputBufferManager()->isExpectedBuffer($handler->getHandlerName());
        }
        return $inProgress;
    }

    /**
     * FileName from given Request processor
     *
     * @param string $path Path part of the URI
     * @return string
     */
    public function processFileName(string $path): string
    {
        $info = pathinfo(urldecode('/' . ltrim($path, '/')));
        $fileName = rtrim('/' . $this->getBasePath(), '/');
        $fileName .= (in_array($info['dirname'], ['', '/'], true) === true) ? '' : '/' . ltrim($info['dirname'], '/');
        $fileName .= (in_array($info['basename'], ['', '.'], true) === true) ? '' : '/' . ltrim($info['basename'], '/');
        $fileName .= ((isset($info['extension']) === false) || ($info['extension'] === '')) ? '/index.html' : '';
        return $fileName;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * BasePath getter
     *
     * @return string
     */
    public function getBasePath(): string
    {
        return $this->basePath;
    }

    /**
     * BasePath setter
     *
     * @param string $basePath
     * @return static Provides fluent interface
     */
    public function setBasePath($basePath): static
    {
        assert(Validators::check($basePath, 'string', 'BasePath'));
        $this->basePath = Strings::trim($basePath, '/');
        return $this;
    }

    /**
     * Handler getter
     *
     * @return StaticContentHandler
     */
    public function getHandler(): StaticContentHandler
    {
        assert(
            Validators::check(
                $this->handler instanceof StaticContentHandler,
                'initialized',
                'handler before get',
            )
        );
        /** @phpstan-var StaticContentHandler $handler */
        $handler = $this->handler;
        return $handler;
    }

    /**
     * Handler setter
     *
     * @param StaticContentHandler $handler
     * @return static Provides fluent interface
     */
    public function setHandler(StaticContentHandler $handler): static
    {
        assert(Validators::check($this->handler, 'null', 'handler before set'));
        $this->handler = $handler;
        return $this;
    }

    /**
     * HandlerFactory getter
     *
     * @return StaticContentHandlerFactoryInterface
     */
    public function getHandlerFactory(): StaticContentHandlerFactoryInterface
    {
        if (isset($this->handlerFactory) === false) {
            $handlerFactory = new StaticContentHandlerFactory();
            $this->setHandlerFactory($handlerFactory);
        }
        return $this->handlerFactory;
    }

    /**
     * HandlerFactory setter
     *
     * @param StaticContentHandlerFactoryInterface $handlerFactory
     * @return static Provides fluent interface
     */
    public function setHandlerFactory(StaticContentHandlerFactoryInterface $handlerFactory): static
    {
        assert(Validators::check(isset($this->handlerFactory), 'uninitialized', 'handlerFactory before set'));
        $this->handlerFactory = $handlerFactory;
        return $this;
    }

    /**
     * OutputBufferManager getter
     *
     * @return OutputBufferManager
     */
    protected function getOutputBufferManager(): OutputBufferManager
    {
        return $this->outputBufferManager;
    }

    /**
     * OutputBufferManager setter
     *
     * @param OutputBufferManager $outputBufferManager
     * @return static Provides fluent interface
     */
    protected function setOutputBufferManager(OutputBufferManager $outputBufferManager): static
    {
        $this->outputBufferManager = $outputBufferManager;
        return $this;
    }

    /**
     * Storage getter
     *
     * @return StorageInterface
     */
    protected function getStorage(): StorageInterface
    {
        return $this->storage;
    }

    /**
     * Storage setter
     *
     * @param StorageInterface $storage
     * @return static Provides fluent interface
     */
    protected function setStorage(StorageInterface $storage): static
    {
        $this->storage = $storage;
        return $this;
    }

    // </editor-fold>
}
