<?php

declare(strict_types=1);

namespace Interitty\StaticContentGenerator\Handler;

use Interitty\StaticContentGenerator\Storage\StorageInterface;

class StaticContentHandlerFactory implements StaticContentHandlerFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(StorageInterface $storage, string $filename, bool $muteOutput = false): StaticContentHandler
    {
        $class = new StaticContentHandler($storage, $filename, $muteOutput);
        return $class;
    }
}
