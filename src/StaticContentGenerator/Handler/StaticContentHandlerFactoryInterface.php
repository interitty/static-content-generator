<?php

declare(strict_types=1);

namespace Interitty\StaticContentGenerator\Handler;

use Interitty\StaticContentGenerator\Storage\StorageInterface;

interface StaticContentHandlerFactoryInterface
{
    /**
     * StaticContentHandler factory
     *
     * @param StorageInterface $storage
     * @param string $filename
     * @param bool $muteOutput [OPTIONAL]
     * @return StaticContentHandler
     */
    public function create(StorageInterface $storage, string $filename, bool $muteOutput = false): StaticContentHandler;
}
