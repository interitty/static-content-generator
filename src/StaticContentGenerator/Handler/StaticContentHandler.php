<?php

declare(strict_types=1);

namespace Interitty\StaticContentGenerator\Handler;

use Interitty\Exceptions\Exceptions;
use Interitty\OutputBufferManager\Handler\HandlerInterface;
use Interitty\StaticContentGenerator\Storage\StorageInterface;
use LogicException;

class StaticContentHandler implements HandlerInterface
{
    /** @var string|null */
    protected ?string $content = null;

    /** @var string */
    protected string $filename;

    /** @var bool */
    protected bool $muteOutput = false;

    /** @var StorageInterface */
    protected StorageInterface $storage;

    /**
     * Constructor
     *
     * @param StorageInterface $storage
     * @param string $filename
     * @param bool $muteOutput [OPTIONAL]
     * @return void
     */
    public function __construct(StorageInterface $storage, string $filename, bool $muteOutput = false)
    {
        $this->setStorage($storage);
        $this->setFilename($filename);
        $this->setMuteOutput($muteOutput);
    }

    /**
     * @inheritdoc
     */
    public function processBegin(): void
    {
        if ($this->content !== null) {
            throw Exceptions::extend(LogicException::class)
                ->setMessage('Content storage was already initialized.');
        }
        $this->content = '';
    }

    /**
     * @inheritdoc
     */
    public function processEnd(): void
    {
        $content = $this->getContent();
        if ($content !== null) {
            $filename = $this->getFilename();
            $this->getStorage()->processPutContent($filename, $content);
            $this->content = null;
        }
    }

    /**
     * @inheritdoc
     */
    public function processManageOutput(string $bufferOutput): string
    {
        $this->content .= $bufferOutput;
        $output = '';
        if ($this->isMuteOutput() === false) {
            $output = $bufferOutput;
        }
        return $output;
    }

    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * @inheritdoc
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Filename getter
     *
     * @return string
     */
    protected function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * Filename setter
     *
     * @param string $filename
     * @return static Provides fluent interface
     */
    protected function setFilename(string $filename): static
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getHandlerName(): string
    {
        $fileName = $this->getFilename();
        return 'StaticContentHandler-' . $fileName;
    }

    /**
     * MuteOutput checker
     *
     * @return bool
     */
    protected function isMuteOutput(): bool
    {
        return $this->muteOutput;
    }

    /**
     * MuteOutput setter
     *
     * @param bool $muteOutput
     * @return static Provides fluent interface
     */
    protected function setMuteOutput(bool $muteOutput): static
    {
        $this->muteOutput = $muteOutput;
        return $this;
    }

    /**
     * Storage getter
     *
     * @return StorageInterface
     */
    protected function getStorage(): StorageInterface
    {
        return $this->storage;
    }

    /**
     * Storage setter
     *
     * @param StorageInterface $storage
     * @return static Provides fluent interface
     */
    protected function setStorage(StorageInterface $storage): static
    {
        $this->storage = $storage;
        return $this;
    }

    // </editor-fold>
}
