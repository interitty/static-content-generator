# Changelog #
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] ##

### Added ###

### Changed ###

### Fixed ###

## [1.0.6] - 2024-12-09 ##

### Changed ###

- PHP 8.3 property static types
- Upgrade dependent packages
- Upgrade license to 2025

## [1.0.5] - 2024-09-01 ##

### Changed ###

- Upgrade dependent packages

### Fixed ###

- Phpstan extension.neon of composer/pcre missing by dg/composer-cleaner

## [1.0.4] - 2024-05-19 ##

### Changed ###

- Update SECURITY key
- Upgrade dependent packages

## [1.0.3] - 2023-12-29 ##

### Changed ###

- Increase minimal PHP to 8.3
- Update security contacts
- Upgrade dependent packages
- Upgrade license to 2024

## [1.0.2] - 2023-04-02 ##

### Added ###

- Docker readonly mode support

## [1.0.1] - 2023-04-02 ##

### Changed ###

- Do not persist on ajax

## [1.0.0] - 2023-04-02 ##

### Added ###

- Static content generator basics
